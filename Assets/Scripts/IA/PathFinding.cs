﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.DataStructures
{
    public abstract class PathFinding
    {
        public abstract List<CellInfo> find_path(Vector3 start_pos, Vector3 target_pos);

        /// <summary> List with the old path, used just to change the color of the cells </summary>
        static protected List<CellInfo> last_path = new List<CellInfo>();
    }
}