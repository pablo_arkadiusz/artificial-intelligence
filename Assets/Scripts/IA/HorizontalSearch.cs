﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.DataStructures
{
    public class HorizontalSearch : PathFinding
    {
        #region Variables

        /// <summary> Ramification level or expanded tree depth (k level or horizon) </summary>
        static public byte ramification_level;
        BoardInfo board_info;
        #endregion

        #region Pseudo code for the Horizontal Search algorithm

        /*
            *    Percibir el estado actual s
            *    Aplicar un método de búsqueda no informado hasta el nivel k y nodos metas.
            *    Sea H el conjunto de nodos hoja hasta el nivel k:
            *         Utilizar h* para determinar el “mejor” nodo 
            *    Ejecutar la primera acción aen el camino que lleva a n
            *    Repetir hasta que el agente se encuentra en un estado meta   
        */

        #endregion

        #region Path-finding methods

        /// <summary> Finds the path between a starting cell and a target cell using the Horizontal Search algorithm </summary>
        override public List<CellInfo> find_path(Vector3 start_pos, Vector3 target_pos)
        {
            board_info = GameManager.instance.GetComponent<BoardManager>().boardInfo;

            CellInfo start_cell = board_info.cell_from_world_point(start_pos);
            CellInfo target_cell = board_info.cell_from_world_point(target_pos);

            //List with open nodes
            List<CellInfo> open_set = new List<CellInfo>();

            //List with nodes that has already been checked
            HashSet<CellInfo> closed_set = new HashSet<CellInfo>();

            //Opens the starting node
            open_set.Add(start_cell);

            //Gets the nodes that are in the selected ramification level. If in this searching there is the target, returns the path to it
            for (byte i = 0; i < ramification_level; ++i)
            {
                open_set = get_neightbours_of_cell_list(open_set, closed_set, target_cell, out bool target_found);
                if (target_found) return retrace_path(start_cell, target_cell);
            }
            
            //Gets the nearest node in the selected ramification level of the horizontal search
            CellInfo selected_node = select_nearest_node(open_set, target_cell);

            open_set.Clear();
            closed_set.Clear();
            return retrace_path(start_cell, selected_node); 
        }
        /// <summary> Returns all the neighbours of a cell list </summary>
        /// <param name="open_set"> List with the cells whose neighbours will be returned </param>
        /// <param name="target_cell"> The cell where the player want go </param>
        /// <param name="target_found"> Out reference. If the target is found it will be true </param>
        private List<CellInfo> get_neightbours_of_cell_list(List<CellInfo> open_set, HashSet<CellInfo> closed_set, CellInfo target_cell, out bool target_found)
        {
            target_found = false;
            List<CellInfo> neighbour_cells = new List<CellInfo>();

            //HashSet with all the nodes that already has been searched
            
            foreach (CellInfo cell in open_set)
            {
                closed_set.Add(cell);
                foreach (CellInfo neighbour_cell in cell.WalkableNeighbours(board_info))
                    if (!closed_set.Contains(neighbour_cell) &&
                    neighbour_cell != null)
                    {
                        neighbour_cell.parent = cell;
                        neighbour_cells.Add(neighbour_cell);
                        closed_set.Add(neighbour_cell);
                        if (neighbour_cell == target_cell)
                            target_found = true; 
                    }
            }
            open_set.Clear();
            return neighbour_cells;
        }

        Color color = Color.red;

        void change_color()
        {
            if (color == Color.red)      
                color = Color.green;
            else if (color == Color.green)
                color = Color.blue;
            else if (color == Color.blue)
                color = Color.red;
        }

        //Return the nearest node to the target in a list of nodes
        private CellInfo select_nearest_node(List<CellInfo> nodes, CellInfo target_node)
        {
            CellInfo nearest_node = null;
            float dist_to_target = float.MaxValue;

            foreach (CellInfo node in nodes)
            {
                //Check if this is the nearest node to the target, and it´s assigned as the nearest if true
                float dist = node.get_distance_between_node(target_node);
                if (dist < dist_to_target)
                {
                    nearest_node = node;
                    dist_to_target = dist;
                }
            }
            return nearest_node;
        }

        

        ///<summary> Retrace the path from the end to the start and then reverse it to make the final path. Returns the complete path </summary>
        List<CellInfo> retrace_path(CellInfo start_cell, CellInfo end_cell)
        {
            List<CellInfo> path = new List<CellInfo>();

            foreach (CellInfo cell in last_path)
                if(cell.cell_sprte != null)
                    cell.cell_sprte.color = Color.white;
            last_path.Clear();
            //Retrace the path from the end to the start
            for (CellInfo current_cell = end_cell; current_cell != start_cell; current_cell = current_cell.parent)
            {
                last_path.Add(current_cell);
                current_cell.cell_sprte.color = Color.blue;
                path.Add(current_cell);
            }

            //Reverse the path to make it go from start to end
            path.Reverse();

            //Returns the complete path
            return path;
        }

        #endregion
    }
}




