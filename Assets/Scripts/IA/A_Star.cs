﻿using System.Collections.Generic;
using UnityEngine;


namespace Assets.Scripts.DataStructures
{
    public class A_Star: PathFinding
    {
        #region Pseudo code for the A* algorithm

        /*
             * OPEN // the set of nodes to be evaluated 
             * CLOSED //the set of nodes already evaluated
             * add the start node to OPEN
             * loop
             *      current = node in OPEN withthe lowest f_cost
             *      remove current from OPEN
             *      add current to CLOSED
             *      
             *      if current is the target node
             *          return
             *      foreach neighbour of the current node
             *          if neighbour is not traversable or neighbour is in closed
             *              skip to the next neighbour
             *      if new path to neightbour is shorter or neightbour is not in open
             *          set f_cost of neightbour
             *          set parent of neightbour to current
             *          if neightbour is not in OPEN
             *              add neightbour to OPEN 
         */

        #endregion

        #region Path-finding methods

        /// <summary> Finds the path between a starting cell and a target cell using the A* algorithm </summary>
        override public List<CellInfo> find_path(Vector3 start_pos, Vector3 target_pos)
        {
            BoardInfo board_info = GameManager.instance.GetComponent<BoardManager>().boardInfo;

            CellInfo start_cell = board_info.cell_from_world_point(start_pos);
            CellInfo target_cell = board_info.cell_from_world_point(target_pos);

            //List with open nodes
            List<CellInfo> open_set = new List<CellInfo>();
            
            //HashSet with all the nodes that already has been searched
            HashSet<CellInfo> closed_set = new HashSet<CellInfo>();

            //Opens the starting node
            open_set.Add(start_cell);

            //While there are open nodes it will open the nodes with lower f_cost
            while(open_set.Count > 0)
            {
                CellInfo current_cell = open_set[0];
                for(int i = 0; i < open_set.Count; ++i)
                    //The node with lower f_cost is chosen, or the one with lower hcost in the case of having same f_cost 
                    if (open_set[i].f_cost < current_cell.f_cost || open_set[i].f_cost == current_cell.f_cost && open_set[i].h_cost < current_cell.h_cost)
                        current_cell = open_set[i];
                
                //The current node is added to closed nodes, because it already has been checked
                open_set.Remove(current_cell);
                closed_set.Add(current_cell);

                //If the final node is found, it returns the path to the end and the search is finished
                if (current_cell == target_cell)
                    return retrace_path(start_cell,target_cell);
                    
                //All target nodes of the target are traversed. And its costs are updated and added to open nodes if necessary.
                foreach (CellInfo neighbour in current_cell.WalkableNeighbours(board_info))
                {
                    if (neighbour == null || closed_set.Contains(neighbour))
                        continue;

                    // Costs of neighbour and the parent nodes are updated if necessary
                    int new_movement_cost_to_neighbour = current_cell.g_cost + (int)current_cell.get_distance_between_node(neighbour);
                    if(new_movement_cost_to_neighbour < neighbour.g_cost || !open_set.Contains(neighbour))
                    {
                        neighbour.g_cost = new_movement_cost_to_neighbour;
                        neighbour.h_cost = (int)neighbour.get_distance_between_node(target_cell);
                        neighbour.parent = current_cell;

                        //Added neighbour to open nodes
                        if (!open_set.Contains(neighbour))
                            open_set.Add(neighbour);
                    }
                }
            }

            //Returns null if there is no path to the target node
            return null;
        }

        ///<summary> Retrace the path from the end to the start and then reverse it to make the final path. Returns the complete path </summary>
        protected List<CellInfo> retrace_path(CellInfo start_cell, CellInfo end_cell)
        {
            foreach (CellInfo cell in last_path)
                if (cell.cell_sprte != null)
                    cell.cell_sprte.color = Color.white;
            last_path.Clear();

            List<CellInfo> path = new List<CellInfo>();

            //Retrace the path from the end to the start
            for (CellInfo current_cell = end_cell; current_cell != start_cell; current_cell = current_cell.parent)
            {
                current_cell.cell_sprte.color = Color.green;
                path.Add(current_cell);
            }

            //Reverse the path to make it go from start to end
            path.Reverse();

            //Returns the complete path
            return path;
        }

        #endregion
    }
}
