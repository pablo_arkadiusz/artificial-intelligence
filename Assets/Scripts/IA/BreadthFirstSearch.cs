﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.Scripts.DataStructures
{
    public class BreadthFirstSearch : PathFinding
    {

        #region Pseudo code for the Breadth First Search algorithm

        /*
           *  método BFS(Grafo,origen):
           *       creamos una cola Q
           *       agregamos origen a la cola Q
           *       marcamos origen como visitado
           *       mientras Q no este vacío:
           *           sacamos un elemento de la cola Q llamado v
           *           para cada vertice w adyacente a v en el Grafo: 
           *               si w no ah sido visitado:
           *                  marcamos como visitado w
           *                  insertamos w dentro de la cola Q
         */

        #endregion

        #region Path-finding methods

        /// <summary> Finds the path between a starting cell and a target cell using the Breadth First Search algorithm </summary>
        override public List<CellInfo> find_path(Vector3 start_pos, Vector3 target_pos)
        {
            BoardInfo board_info = GameManager.instance.GetComponent<BoardManager>().boardInfo;

            CellInfo start_cell = board_info.cell_from_world_point(start_pos);
            CellInfo target_cell = board_info.cell_from_world_point(target_pos);

            //List with open nodes
            List<CellInfo> open_set = new List<CellInfo>();

            //List with nodes that has already been searched
            List<CellInfo> closed_set = new List<CellInfo>();

            //Opens the starting node
            open_set.Add(start_cell);

            //While there are open nodes it will open the nodes with lower f_cost
            while (open_set.Count > 0)
            {
                //If it found the target cell returns the final path between the starting position and the end position
                if (open_set.Contains(target_cell))
                    return retrace_path(start_cell, target_cell);

                //Auxiliar open set
                List<CellInfo> new_open_set = new List<CellInfo>();

                //Opens all the neighbours of all the elements in the open set, update the parent of all of them and add all the neigbours to the auxiliar open set
                //And update the closed set
                foreach (CellInfo cell in open_set)
                    foreach (CellInfo neighbour_cell in cell.WalkableNeighbours(board_info))
                        if ((neighbour_cell != null || neighbour_cell == target_cell) && (!new_open_set.Contains(neighbour_cell) && !open_set.Contains(neighbour_cell) && neighbour_cell != cell.parent))
                            if (!closed_set.Contains(neighbour_cell))
                            {
                                neighbour_cell.parent = cell;
                                new_open_set.Add(neighbour_cell);
                                closed_set.Add(neighbour_cell);
                            }

                //The open set nodes now will be the neighbours of the old open set
                open_set.Clear();
                open_set = new_open_set;
            }

            //Returns null if there is no path to the target node
            return null;
        }


        ///<summary> Retrace the path from the end to the start and then reverse it to make the final path. Returns the complete path </summary>
        protected List<CellInfo> retrace_path(CellInfo start_cell, CellInfo end_cell)
        {

            foreach (CellInfo cell in last_path)
                if (cell.cell_sprte != null)
                    cell.cell_sprte.color = Color.white;
            last_path.Clear();


            List<CellInfo> path = new List<CellInfo>();

            //Retrace the path from the end to the start
            for (CellInfo current_cell = end_cell; current_cell != start_cell; current_cell = current_cell.parent)
            {
                current_cell.cell_sprte.color = Color.green;
                path.Add(current_cell);
            }
            
            //Reverse the path to make it go from start to end
            path.Reverse();

            //Returns the complete path
            return path;
        }
        #endregion
    }
}


