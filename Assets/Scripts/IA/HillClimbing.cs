﻿using System.Collections.Generic;
using UnityEngine;


namespace Assets.Scripts.DataStructures
{

    public class HillClimbing : PathFinding
    {
        #region Pseudo code for the Hill Climbing algorithm

        /*
            *    Define the current state as an initial state
            *    Loop until the goal state is achieved or no more operators can be applied on the current state:
            *       Apply an operation to current state and get a new state
            *       Compare the new state with the goal
            *       Quit if the goal state is achieved
            *       Evaluate new state with heuristic function and compare it with the current state
            *       If the newer state is closer to the goal compared to current state, update the current state
        */

        #endregion

        #region Path-finding methods

        public override List<CellInfo> find_path(Vector3 start_pos, Vector3 target_pos)
        {
            

            List<CellInfo> path = new List<CellInfo>();
            path.Add(find_next_movement(start_pos, target_pos));
            return path;
        }

        /// <summary> Finds the nearest node to the target </summary>
        private CellInfo find_next_movement(Vector3 start_pos, Vector3 target_pos)
        {
            foreach (CellInfo cell in last_path)
                if(cell.cell_sprte != null)
                    cell.cell_sprte.color = Color.white;
            last_path.Clear();
            BoardInfo board_info = GameManager.instance.GetComponent<BoardManager>().boardInfo;

            CellInfo start_cell = board_info.cell_from_world_point(start_pos);
            CellInfo target_cell = board_info.cell_from_world_point(target_pos);

            //Get the nearest node to the target of the neighbour nodes
            CellInfo selected_node = select_nearest_node(start_cell.WalkableNeighbours(board_info), target_cell);
            selected_node.cell_sprte.color = Color.blue;
            last_path.Add(selected_node);
            return selected_node;
        }

        /// <summary> Return the nearest node to the target in a list of nodes </summary>
        private CellInfo select_nearest_node(CellInfo[] nodes, CellInfo target_node)
        {
            CellInfo nearest_node = null;
            float dist_to_target = float.MaxValue;

            foreach (CellInfo node in nodes)
                if (node != null)
                {
                    //Check if this is the nearest node to the target, and it´s assigned as the nearest if true
                    float dist = node.get_distance_between_node(target_node);
                    if (dist < dist_to_target)
                    {
                        nearest_node = node;
                        dist_to_target = dist;
                    }
                }
            return nearest_node;
        }

        #endregion
    }
}