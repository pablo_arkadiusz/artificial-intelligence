﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameButtonEvents : MonoBehaviour
{
    private void Start()
    {
        Button this_btn = GetComponent<Button>();
        this_btn.onClick.AddListener(LevelManager.instance.load_menu_scene);


    }
}
