﻿
using UnityEngine;
using UnityEngine.UI;

public class MenuButtonEvents : MonoBehaviour
{
    void Start()
    {
        Button this_btn = GetComponent<Button>();
        this_btn.onClick.AddListener(Assets.Scripts.GameManager.instance.set_up_scene);
        this_btn.onClick.AddListener(LevelManager.instance.load_game_scene);
    }
}
