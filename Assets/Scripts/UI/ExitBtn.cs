﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitBtn : MonoBehaviour
{
    void Start()
    {
        Button this_btn = GetComponent<Button>();
        this_btn.onClick.AddListener(LevelManager.instance.quit_game);
    }

}
