﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class Loader : MonoBehaviour
    {
        public bool Planner=false;
        void Awake()
        {
            GameManager game_manager = GameManager.instance;
            game_manager.InitGame();
        }
    }
}