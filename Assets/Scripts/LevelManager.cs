﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance = null;              //Static instance of LevelManager which allows it to be accessed by any other script.


    private void Awake()
    {
        // Singleton pattern, meaning there can only ever be one instance of a LevelManager.
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    public enum Scene {GameScene,MenuScene}
    public void load_game_scene() => SceneManager.LoadScene(Scene.GameScene.ToString());
    
    public void load_menu_scene() => SceneManager.LoadScene(Scene.MenuScene.ToString());

    public void quit_game() => Application.Quit();
    
}
