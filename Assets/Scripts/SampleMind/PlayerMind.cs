﻿using Assets.Scripts.DataStructures;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.SampleMind
{
    public class PlayerMind : AbstractPathMind
    {
        /// <summary>
        /// Path finding system that the player's mind has. It can be offline: A_Star or BreadthFirstSearch or online: HillClimbing or HorizontalSearch
        /// </summary>
        private PathFinding path_finding;

        /// <summary> The complete path from the player node to the target node </summary>
        public List<CellInfo> path;

        /// <summary> The target that player will follow </summary>
        public Transform target;

        [HideInInspector] public List<GameObject> enemies;

        /// <summary> All the online pathfinding methods used to find the enemies </summary>
        public enum Online_Algorithms {Hill_Climbing, Horizontal_Search}
        /// <summary> All the offline pathfinding methods used to find the goal target </summary>
        public enum Offline_Algorithms {A_Star, Breadth_First_Search}

        /// <summary> Online searching or offline ? </summary>
        bool online_searching = true;

        //private void Awake() { Debug.LogError("AWAKED"); enemies = new List<GameObject>(); }

        void Start()
        {
            switch(GameManager.instance.selected_online_search)
            {
                case Online_Algorithms.Horizontal_Search:
                    path_finding = new HorizontalSearch();
                    break;
                case Online_Algorithms.Hill_Climbing:
                    path_finding = new HillClimbing();
                    break;
            }

            set_target(GameObject.Find("Goal(Clone)").transform);
            try_to_change_to_offline_searching();
        }

        public void set_target(Transform new_target) => target = new_target;
        
        public override void Repath(){}

        /// <summary> Choose the next movement to follow the route to the target, uses diferent approaches for every path finding method</summary>
        public override Locomotion.MoveDirection GetNextMove(BoardInfo boardInfo, CellInfo current_cell, CellInfo[] goals)
        {
            CellInfo next_target = null;

            
                if (online_searching)
                    switch (GameManager.instance.selected_online_search)
                    {
                        case Online_Algorithms.Hill_Climbing:
                            //The next movement will follow the nearest target
                            Vector2 nearest_enemy = select_nearest_enemy_pos();
                            next_target = path_finding.find_path(transform.position, nearest_enemy)[0];
                            break;
                        case Online_Algorithms.Horizontal_Search:

                            //Find a horizontal path to the nearest enemy (the path lenght depends of the HorizontalSearch´s ramification level) 
                            if (path == null || path.Count == 0)
                            {
                                nearest_enemy = select_nearest_enemy_pos();
                                path = path_finding.find_path(transform.position, nearest_enemy);
                            }

                            CellInfo[] neighbours = current_cell.WalkableNeighbours(boardInfo);

                            //Gets all the walkable neighbours of the cell where the player is standing 
                            neighbours = current_cell.WalkableNeighbours(boardInfo);

                            //Look through the neighbour to check which of them is the next cell of the path
                            for (byte i = 0; i < neighbours.Length; ++i)
                                if (neighbours[i] != null && path.Count > 0 && neighbours[i] == path[0])
                                {
                                    next_target = neighbours[i];
                                    path.Remove(next_target);
                                }
                            break;
                    }
                else
                    switch (GameManager.instance.selected_offline_search)
                    {
                        case Offline_Algorithms.A_Star:
                        case Offline_Algorithms.Breadth_First_Search:

                            //Assign the path to the target
                            if (path == null)
                                path = path_finding.find_path(transform.position, target.position);

                            //Gets all the walkable neighbours of the cell where the player is standing 
                            CellInfo[] neighbours = current_cell.WalkableNeighbours(boardInfo);

                            //Look through the neighbour to check which of them is the next cell of the path
                            for (byte i = 0; i < neighbours.Length; ++i)
                                if (neighbours[i] != null && path.Count > 0 && neighbours[i] == path[0])
                                {
                                    next_target = neighbours[i];
                                    path.Remove(neighbours[i]);
                                }
                            break;
                    }

            if (next_target == null)
            {
                GameManager.instance.seed = Random.Range(0, (int)Time.time);
                SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
            }

            //Finds what is the next movement to the selected target
            if (next_target.RowId + 1 == current_cell.RowId)
                    return Locomotion.MoveDirection.Down;
                else if (next_target.RowId - 1 == current_cell.RowId)
                    return Locomotion.MoveDirection.Up;
                else if (next_target.ColumnId + 1 == current_cell.ColumnId)
                    return Locomotion.MoveDirection.Left;
                else // if(next_target.ColumnId - 1 == current_cell.ColumnId)
                    return Locomotion.MoveDirection.Right;
        }

        private Vector2 select_nearest_enemy_pos()
        {
            GameObject nearest_enemy = null;
            float dist_to_enemy = float.MaxValue;

            Debug.LogError("NUMBER OF ENEMIES IS : " + enemies.Count);

            foreach (GameObject enemy in enemies)
            {
                Debug.LogError("ENEMY FOUND: ");

                float dist = (transform.position - enemy.transform.position).magnitude;
                if (dist_to_enemy > dist)
                {
                    dist_to_enemy = dist;
                    nearest_enemy = enemy;
                }
            }
            return nearest_enemy.transform.position;
        }

        /// <summary> If there arent more enemies alive, the path finding method change to an offline one </summary>
        public void try_to_change_to_offline_searching()
        {
           
            if (enemies.Count == 0)
            {
                
                GameManager.instance.goal_enabled = true; //The goal node is set to active

                //The searching method is changed to offline
                online_searching = false;
                switch(GameManager.instance.selected_offline_search)
                {
                    case Offline_Algorithms.A_Star:
                        path_finding = new A_Star();
                        break;
                    case Offline_Algorithms.Breadth_First_Search:
                        path_finding = new BreadthFirstSearch();
                        break;
                }
            }
        }


        public void reset_path() => path = null;
        
    }
}