﻿using Assets.Scripts.DataStructures;
using UnityEngine;

namespace Assets.Scripts.SampleMind
{
    public class RandomMind : AbstractPathMind {
        public override void Repath()
        {
            
        }


        /// <summary>
        /// Se asigna la dirección hacia la que decidirá moverse el RandomMind. Es tandom, y no se moverá nunca en la dirección de los obstáculos 
        /// </summary>
        /// <returns>Dirección hacia la que debe moverse</returns>
        public override Locomotion.MoveDirection GetNextMove(BoardInfo boardInfo, CellInfo currentPos, CellInfo[] goals)
        {
            //Se recoge en un array las casillas de los vecinos. Los vecino que sean unwalkable serán null.
            CellInfo[] walkable_neighbours = currentPos.WalkableNeighbours(boardInfo);

            //Se asigna la dirección random de movimiento a una casilla de esa zona no sea unwalkable (walkable_neighbours[index] != null)
            while (true)
            {
                var val = Random.Range(0, 4);
                if (val == 0 && walkable_neighbours[0] != null) return Locomotion.MoveDirection.Up;
                else if (val == 1 && walkable_neighbours[1] != null) return Locomotion.MoveDirection.Right;
                else if(val == 2 && walkable_neighbours[2] != null) return Locomotion.MoveDirection.Down;
                else if(val == 3 && walkable_neighbours[3] != null) return Locomotion.MoveDirection.Left;
            }
        }
    }
}
