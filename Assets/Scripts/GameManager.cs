﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Collections;
using System;

namespace Assets.Scripts
{
    using Online_Path_Finding = SampleMind.PlayerMind.Online_Algorithms;
    using Offline_Path_Finding = SampleMind.PlayerMind.Offline_Algorithms;

    public class GameManager : MonoBehaviour
    {
        private ToggleGroup online_toggle_group;
        private ToggleGroup offline_toggle_group;
        private Slider row_slider;
        private Slider column_slider;
        private Slider enemy_num_slider;


        public static GameManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
        public BoardManager BoardManager { get; set; }                      //Store a reference to our Board which will set up the level.
        public int seed;
        private float seed_time = 0;
        public bool ForPlanner = false;
        public int numEnemies;
        public byte horizontal_level;
        public Offline_Path_Finding selected_offline_search;
        public Online_Path_Finding selected_online_search;


        public bool goal_enabled;



        void Awake()
        {

            StartCoroutine(update_time());
            //Check if instance already exists
            if (instance == null)
            {
                //if not, set instance to this
                instance = this;
                
            }
            //If instance already exists and it's not this:
            else if (instance != this)

                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
                Destroy(gameObject);

            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);

            //Get a component reference to the attached BoardManager script
            this.BoardManager = GetComponent<BoardManager>();
 
        }

        IEnumerator update_time()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();
                seed_time += 1;
            }
        }


        public void set_up_scene()
        {
            Toggle online_toggle = online_toggle_group.ActiveToggles().First();

            if (online_toggle.name == "Hill Climbing Toggle")
                selected_online_search = Online_Path_Finding.Hill_Climbing;       
            else
            {
                selected_online_search = Online_Path_Finding.Horizontal_Search;
                Slider horizontal_depth_slider = Array.Find<Slider>(online_toggle.GetComponentsInChildren<Slider>(), elem => elem.name.Contains("Horizon Depth"));
                DataStructures.HorizontalSearch.ramification_level = (byte)horizontal_depth_slider.value;
            }

            Toggle offline_toggle = offline_toggle_group.ActiveToggles().First();
            selected_offline_search = (offline_toggle.name == "A-Star Toggle") ? Offline_Path_Finding.A_Star : Offline_Path_Finding.Breadth_First_Search;

            numEnemies = (int) enemy_num_slider.value;
            BoardManager.rows = (int) row_slider.value;
            BoardManager.columns = (int) column_slider.value;
        }

        // called first
        void OnEnable() => SceneManager.sceneLoaded += OnSceneLoaded;
        

        // called second
        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (scene.name == LevelManager.Scene.GameScene.ToString())
            {
                CharacterBehaviour character = GameObject.Find("Character").GetComponent<CharacterBehaviour>();
                character.BoardManager = BoardManager;
                character.SetCurrentTarget(BoardManager.boardInfo.Exit);
                seed = UnityEngine.Random.Range(0, (int)seed_time);
            }
            else if (scene.name == LevelManager.Scene.MenuScene.ToString())
            {
                GameManager.instance.goal_enabled = false;

                load_components();
            }
            
        }

        private void load_components()
        {
            GameObject[] toggle_groups = GameObject.FindGameObjectsWithTag("Toggle");

            foreach (GameObject toggle in toggle_groups)
                if (toggle.name.Contains("Online"))
                    online_toggle_group = toggle.GetComponent<ToggleGroup>();
                else
                    offline_toggle_group = toggle.GetComponent<ToggleGroup>();

            GameObject[] sliders = GameObject.FindGameObjectsWithTag("Slider");


            foreach (GameObject slider in sliders)
                if (slider.name.Contains("Row"))
                    row_slider = slider.GetComponent<Slider>();
                else if (slider.name.Contains("Column"))
                    column_slider = slider.GetComponent<Slider>();
                else if (slider.name.Contains("Enemy"))
                    enemy_num_slider = slider.GetComponent<Slider>();
        }

        //Initializes the game for each level.
        public void InitGame()
        {
            //Call the SetupScene function of the BoardManager script, pass it current level number.
            BoardManager.SetupScene(this.seed, this.ForPlanner,numEnemies);
            BoardManager.GenerateMap();    
        }
    }
}