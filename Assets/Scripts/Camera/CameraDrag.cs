﻿using UnityEngine;

public class CameraDrag : MonoBehaviour
{
    public int camera_drag_speed = 50;

    private float board_width, board_height;
    private float half_size_tile = Assets.Scripts.BoardManager.TileSize / 2;
    Plane plane;
    private void Start()
    {
        // Create a logical plane perpendicular to z and at (0,0,0):
        plane = new Plane(Vector3.forward, new Vector3(0, 0, 0));

        board_height = Assets.Scripts.GameManager.instance.BoardManager.rows - half_size_tile;
        board_width = Assets.Scripts.GameManager.instance.BoardManager.columns - half_size_tile;

        send_camera_to_start_pos();
       
    }

    private void Update()
    {
       
        float hit;

        //Raycast to shot from a point of camera to the plane, to get the hit point and therefore know the camera limits
        Ray ray;

        //Camera margin points
        float left_margin = 0, top_margin = 0, right_margin = 0, bot_margin = 0;

        //Calculate all the margins of the camera view
        ray = Camera.main.ViewportPointToRay(new Vector3(0, 0, 0));// bottom left ray
        if (plane.Raycast(ray, out hit))
        {
            left_margin = ray.GetPoint(hit).x;
            bot_margin = ray.GetPoint(hit).y;
        }

        ray = Camera.main.ViewportPointToRay(new Vector3(1, 1, 0)); // top right ray
        if (plane.Raycast(ray, out hit))
        {
            right_margin = ray.GetPoint(hit).x;
            top_margin = ray.GetPoint(hit).y;
        }

        if (Input.GetMouseButton(0))
        {
            //Get the mouse axis
            float y_axis = Input.GetAxis("Mouse Y");
            float x_axis = Input.GetAxis("Mouse X");

            float speed = camera_drag_speed * Time.deltaTime;

            //Makes the x and y camera drag if the camera is in the right position
            if (should_x_drag(right_margin, left_margin, x_axis))           
                Camera.main.transform.position -= new Vector3(x_axis * speed,0, 0);  

            if (should_y_drag(top_margin,bot_margin,y_axis))
                Camera.main.transform.position -= new Vector3(0 ,y_axis * speed, 0);     
        }
    }

    /// <summary> If true the camera should can be dragged in the x axis </summary>
    /// <param name="right_margin"> right margin of the camera view </param>
    /// <param name="left_margin">  left margin of the camera view </param>
    /// <param name="x_axis"> x axis of the mouse movement </param>
    private bool should_x_drag(float right_margin, float left_margin, float x_axis) =>
        right_margin < board_width && left_margin > -half_size_tile ||
        right_margin > board_width  && x_axis > 0 || left_margin < -half_size_tile && x_axis < 0;

    
    /// <summary> If true the camera should can be dragged in the y axis </summary>
    /// <param name="top_margin"> top margin of the camera view </param>
    /// <param name="bot_margin"> bottom margin of the camera view </param>
    /// <param name="y_axis"> y axis of the mouse movement </param>
    private bool should_y_drag(float top_margin, float bot_margin, float y_axis) =>
        top_margin < board_height && bot_margin > -0.5 ||
        top_margin > board_height && y_axis > 0 || bot_margin < -0.5 && y_axis < 0;


    private void send_camera_to_start_pos()
    {
        Ray ray;
        float hit;
        float left_margin = 0, top_margin = 0, right_margin = 0, bot_margin = 0;

        //Calculate all the margins of the camera view
        ray = Camera.main.ViewportPointToRay(new Vector3(0, 0, 0));// bottom left ray
        if (plane.Raycast(ray, out hit))
        {
            left_margin = ray.GetPoint(hit).x;
            bot_margin = ray.GetPoint(hit).y;
        }

        ray = Camera.main.ViewportPointToRay(new Vector3(1, 1, 0)); // top right ray
        if (plane.Raycast(ray, out hit))
        {
            right_margin = ray.GetPoint(hit).x;
            top_margin = ray.GetPoint(hit).y;
        }

        Camera cam = Camera.main;
        float height = top_margin - bot_margin;
        float width = right_margin - left_margin;

        transform.position = new Vector3(width / 2 - half_size_tile, height / 2 - half_size_tile, transform.position.z);
    }
}